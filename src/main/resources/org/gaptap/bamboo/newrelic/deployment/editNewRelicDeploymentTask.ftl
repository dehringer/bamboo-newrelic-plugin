
[@ww.select labelKey='newrelic.config.deployment.apiKey.options' name='apiKeyOption'
                                        listKey='key' listValue='value' toggle='true'
                                        list=apiKeyOptions /]

[@ui.bambooSection dependsOn="apiKeyOption" showOn="0"]
	[#if sharedApiKeys.size() == 0]
		[@ui.messageBox type="warning" titleKey="newrelic.config.deployment.apiKey.options.none.configured.title"]
		    [@ww.text name="newrelic.config.deployment.apiKey.options.none.configured.message"]
		    [/@ww.text]&nbsp;
		    [#if fn.hasAdminPermission()]
			    [@ww.text name="newrelic.config.deployment.apiKey.options.none.configured.add"]
			        [@ww.param]${req.contextPath}/admin/newrelic/configuration.action[/@ww.param]
			    [/@ww.text]
		    [/#if]
		[/@ui.messageBox]
	[/#if]
    [@ww.select labelKey='newrelic.config.deployment.apiKey.options.shared' name='sharedApiKeyId'
                                        listKey='key' listValue='value' toggle='true'
                                        list=sharedApiKeys /]
[/@ui.bambooSection]

[@ui.bambooSection dependsOn="apiKeyOption" showOn="1"]
	[@ww.textfield labelKey='newrelic.config.deployment.apiKey' name='apiKey' required='true' cssClass="long-field" /]
[/@ui.bambooSection]

[@ww.textfield labelKey='newrelic.config.deployment.appName' name='appName' required='true' /]

[@ui.bambooSection titleKey='newrelic.config.deployment.advanced' collapsible=true isCollapsed=!(customProxy?has_content)]

    [@ww.checkbox labelKey='newrelic.config.deployment.customProxy' descriptionKey='newrelic.config.deployment.customProxy.description' name='customProxy' toggle='true' /]
    
    [@ui.bambooSection dependsOn="customProxy" showOn='true']
    	[@ww.select labelKey='newrelic.config.deployment.proxy.options' name='proxyOption'
                                        listKey='key' listValue='value' toggle='true'
                                        list=proxyOptions /]
                                        
        [@ui.bambooSection dependsOn="proxyOption" showOn='1']
			[@ww.textfield labelKey='newrelic.config.deployment.proxy.host' name='proxyHost' required='false' /]
			[@ww.textfield labelKey='newrelic.config.deployment.proxy.port' name='proxyPort' required='false' cssClass="short-field"/]
		[/@ui.bambooSection]
		
		[@ui.bambooSection dependsOn="proxyOption" showOn="0"]
			[#if sharedProxies.size() == 0]
				[@ui.messageBox type="warning" titleKey="newrelic.config.deployment.proxy.options.none.configured.title"]
				    [@ww.text name="newrelic.config.deployment.proxy.options.none.configured.message"]
				    [/@ww.text]&nbsp;
				    [#if fn.hasAdminPermission()]
					    [@ww.text name="newrelic.config.deployment.proxy.options.none.configured.add"]
					        [@ww.param]${req.contextPath}/admin/newrelic/configuration.action[/@ww.param]
					    [/@ww.text]
				    [/#if]
				[/@ui.messageBox]
			[/#if]
		    [@ww.select labelKey='newrelic.config.deployment.proxy.options.shared' name='sharedProxyId'
		                                        listKey='key' listValue='value' toggle='true'
		                                        list=sharedProxies /]
		[/@ui.bambooSection]		
		
	[/@ui.bambooSection]
[/@ui.bambooSection]