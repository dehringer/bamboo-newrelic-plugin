

<html>
<head>
	[@ui.header pageKey="newrelic.admin.sharedConfig.title" title=true /]
	<meta name="decorator" content="adminpage">
    ${webResourceManager.requireResource("org.gaptap.bamboo.newrelic.newrelic-plugin:newrelic-plugin-resources")}
</head>
<body>
	[@ui.header pageKey="newrelic.admin.sharedConfig.heading" /]
	<p>
		<img src="${req.contextPath}/download/resources/org.gaptap.bamboo.newrelic.newrelic-plugin:newrelic-plugin-resources/images/NewRelic-logo-square-RGBHEX.png" style="float: left; margin-right: 5px" width="32" height="32" />[@ww.text name='newrelic.admin.overview.description' /]
	</p>
	[@ww.actionmessage /]
	[@ui.clear/]
	
	[@dj.tabContainer headingKeys=["newrelic.admin.sharedApiKey.tab.heading", "newrelic.admin.sharedProxy.tab.heading"] selectedTab='${selectedTab!}']
	    [@dj.contentPane labelKey="newrelic.admin.sharedApiKey.tab.heading"]
	        [@sharedApiKeysTab/]
	    [/@dj.contentPane]
	    [@dj.contentPane labelKey="newrelic.admin.sharedProxy.tab.heading"]
	        [@sharedProxiesTab/]
	    [/@dj.contentPane]
	[/@dj.tabContainer]
</body>
</html>

[#macro sharedApiKeysTab]	
	<div class="toolbar">
		<div class="aui-toolbar inline">
			<ul class="toolbar-group">
				<li class="toolbar-item">
					<a class="toolbar-trigger" href="[@ww.url action='addSharedApiKey' namespace='/admin/newrelic' /]">[@ww.text name='newrelic.admin.sharedApiKey.add' /]</a>
				</li>
			</ul>
		</div>
	</div>
	<p>[@ww.text name='newrelic.admin.sharedApiKey.description' /]</p>
	[@ui.bambooPanel titleKey='newrelic.admin.sharedApiKey.list.heading']
		[#if apiKeys!?size > 0]	
		<table id="credentials" class="aui" width="100%">
			<thead><tr>
				<th class="labelPrefixCell">[@ww.text name='newrelic.admin.list.heading.sharedApiKey' /]</th>
				<th class="valueCell">[@ww.text name='newrelic.admin.list.heading.configuration' /]</th>
				<th class="operations">[@ww.text name='newrelic.admin.list.heading.operations' /]</th>
			</tr></thead>
			<tbody>
				[#foreach apiKey in apiKeys]
					<tr>
						<td class="labelPrefixCell">
							${apiKey.name}<br />
							[#if apiKey.description?has_content]
								<span class="subGrey">${apiKey.description}</span>
							[/#if]
						</td>
						<td class="valueCell">
							<b>[@ww.text name='newrelic.admin.sharedApiKey.list.apiKey' /]:</b> ${apiKey.apiKey}<br />
						</td>
						<td class="operations">
							<a href="${req.contextPath}/admin/newrelic/editSharedApiKey.action?apiKeyId=${apiKey.ID}">[@ww.text name='newrelic.admin.sharedApiKey.edit' /]</a>
							| <a href="${req.contextPath}/admin/newrelic/confirmDeleteSharedApiKey.action?apiKeyId=${apiKey.ID}">[@ww.text name='newrelic.admin.sharedApiKey.delete' /]</a>
						</td>
					</tr>
				[/#foreach]
			</tbody>
		</table>			
		[#else]
			[@ui.messageBox type='warning' titleKey='newrelic.admin.sharedApiKey.none']
				[@ww.text name='newrelic.admin.sharedApiKey.none.help' /]
			[/@ui.messageBox]
		[/#if]
	[/@ui.bambooPanel]
[/#macro]

[#macro sharedProxiesTab]	
	<div class="toolbar">
		<div class="aui-toolbar inline">
			<ul class="toolbar-group">
				<li class="toolbar-item">
					<a class="toolbar-trigger" href="[@ww.url action='addSharedProxy' namespace='/admin/newrelic' /]">[@ww.text name='newrelic.admin.sharedProxy.add' /]</a>
				</li>
			</ul>
		</div>
	</div>
	<p>[@ww.text name='newrelic.admin.sharedProxy.description' /]</p>
	[@ui.bambooPanel titleKey='newrelic.admin.sharedProxy.list.heading']
		[#if proxies!?size > 0]	
		<table id="credentials" class="aui" width="100%">
			<thead><tr>
				<th class="labelPrefixCell">[@ww.text name='newrelic.admin.list.heading.sharedProxy' /]</th>
				<th class="valueCell">[@ww.text name='newrelic.admin.list.heading.configuration' /]</th>
				<th class="operations">[@ww.text name='newrelic.admin.list.heading.operations' /]</th>
			</tr></thead>
			<tbody>
				[#foreach proxy in proxies]
					<tr>
						<td class="labelPrefixCell">
							${proxy.name}<br />
							[#if proxy.description?has_content]
								<span class="subGrey">${proxy.description}</span>
							[/#if]
						</td>
						<td class="valueCell">
							<b>[@ww.text name='newrelic.admin.sharedProxy.list.host' /]:</b> ${proxy.host}<br />
							<b>[@ww.text name='newrelic.admin.sharedProxy.list.port' /]:</b> ${proxy.port}<br />
						</td>
						<td class="operations">
							<a href="${req.contextPath}/admin/newrelic/editSharedProxy.action?proxyId=${proxy.ID}">[@ww.text name='newrelic.admin.sharedProxy.edit' /]</a>
							| <a href="${req.contextPath}/admin/newrelic/confirmDeleteSharedProxy.action?proxyId=${proxy.ID}">[@ww.text name='newrelic.admin.sharedProxy.delete' /]</a>
						</td>
					</tr>
				[/#foreach]
			</tbody>
		</table>			
		[#else]
			[@ui.messageBox type='warning' titleKey='newrelic.admin.sharedProxy.none']
				[@ww.text name='newrelic.admin.sharedProxy.none.help' /]
			[/@ui.messageBox]
		[/#if]
	[/@ui.bambooPanel]
[/#macro]
