/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment.admin;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;

/**
 * @author David Ehringer
 */
@SuppressWarnings({ "serial", "unchecked" })
public class BaseAdminAction extends BambooActionSupport implements
		GlobalAdminSecurityAware {

	/**
	 * ADD Mode type string value
	 */
	public static final String ADD = "add";

	/**
	 * EDIT Mode type string value
	 */
	public static final String EDIT = "edit";

	private String action;
	private String mode;
	// default tab
	
	@Override
	public String doDefault() throws Exception {
		if (StringUtils.isNotBlank(getAction())) {
			addActionMessage(getText(
					"newrelic.admin.messages.action",
					new String[] { getText("newrelic.admin.messages.action."
							+ getAction()) }));
		}
		return SUCCESS;
	}
	
	public String doAdd() {
		setMode(ADD);
		return ADD;
	}
	
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
