/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.gaptap.bamboo.newrelic.ConnectionParams;
import org.gaptap.bamboo.newrelic.NewRelicApiException;

import com.atlassian.bamboo.build.logger.BuildLogger;

/**
 * @author David Ehringer
 */
public class DeploymentNotifier {

    private static final String URL = "https://rpm.newrelic.com/deployments.xml";

    public String notify(DeploymentInfo info, BuildLogger buildLogger) throws NewRelicApiException {
        HttpClient client = new DefaultHttpClient();
        return doNotify(client, info, buildLogger);
    }

    public String notify(DeploymentInfo info, ConnectionParams connectionParams, BuildLogger buildLogger)
            throws NewRelicApiException {
        HttpClient client = new DefaultHttpClient();
        HttpHost proxy = new HttpHost(connectionParams.getProxyHost(), connectionParams.getProxyPort());
        client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
        return doNotify(client, info, buildLogger);
    }

    private String doNotify(HttpClient client, DeploymentInfo info, BuildLogger buildLogger)
            throws NewRelicApiException {
        HttpPost post = new HttpPost(URL);
        post.setHeader("x-api-key", info.getApiKey());

        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("deployment[app_name]", info.getAppName()));
        urlParameters.add(new BasicNameValuePair("deployment[description]", info.getDescription()));
        urlParameters.add(new BasicNameValuePair("deployment[revision]", info.getRevision()));
        urlParameters.add(new BasicNameValuePair("deployment[changelog]", info.getChangelog()));
        urlParameters.add(new BasicNameValuePair("deployment[user]", info.getUser()));

        try {
            post.setEntity(new UrlEncodedFormEntity(urlParameters));
        } catch (UnsupportedEncodingException e) {
            throw new NewRelicApiException("Unable to encode form data to POST to the New Relic API", e);
        }

        StringBuilder result = new StringBuilder();
        try {
            HttpResponse response = client.execute(post);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            int responseCode = response.getStatusLine().getStatusCode();
            if (201 == responseCode) {
                buildLogger.addBuildLogEntry("Successfully notified New Relic of deployment");
            } else {
                logErrorsForNotification(responseCode, result.toString(), info.getAppName(), buildLogger);
            }
        } catch (IOException e) {
            buildLogger.addErrorLogEntry("Exception occurred when calling New Relic API: " + e.getMessage());
        }

        return result.toString();
    }

    private void logErrorsForNotification(int responseCode, String result, String appName, BuildLogger buildLogger) {
        buildLogger.addErrorLogEntry("New Relic API response (HTTP " + responseCode + "): ");
        if (403 == responseCode) {
            buildLogger.addErrorLogEntry("API returned unauthorized. Please verify the API key used in your 'New Relic Deployment Notifier' task.");
        } else if (422 == responseCode) {
            buildLogger.addErrorLogEntry("Unable to update deployment information for application '" + appName
                    + "'. Please verify that your application name is configured correctly.");
        } else {
            buildLogger.addErrorLogEntry(result);
            throw new NewRelicApiException("Unable to notify New Relic of deployment. Please check the logs for details.");
        }
    }
}