/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment;

import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.APP_NAME;

import org.gaptap.bamboo.newrelic.AbstractNewRelicTask;
import org.gaptap.bamboo.newrelic.ConnectionParams;
import org.gaptap.bamboo.newrelic.NewRelicApiException;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.RerunBuildTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;

/**
 * @author David Ehringer
 */
public class NewRelicDeploymentTask extends AbstractNewRelicTask {

    @Override
    public TaskResult execute(@NotNull CommonTaskContext commonTaskContext) throws TaskException {
        BuildLogger buildLogger = commonTaskContext.getBuildLogger();
        buildLogger.addBuildLogEntry("Notifying New Relic of deployment");

        DeploymentInfo info = new DeploymentInfo();
        populateUserSuppliedData(commonTaskContext, info);

        String user = getUser(commonTaskContext);
        info.setUser(user);

        if (isDeploymentProject(commonTaskContext)) {
            DeploymentTaskContext deploymentContext = (DeploymentTaskContext) commonTaskContext;

            String version = deploymentContext.getDeploymentContext().getDeploymentVersion().getName();
            info.setRevision(version);

            String description = createDeploymentDescription(deploymentContext);
            info.setDescription(description);
        } else {
            TaskContext taskContext = (TaskContext) commonTaskContext;

            String buildKey = taskContext.getBuildContext().getBuildResultKey();
            info.setRevision("Build: " + buildKey);

            String description = createBuildDescription(taskContext);
            info.setDescription(description);
        }

        DeploymentNotifier notifier = new DeploymentNotifier();
        try {
            buildLogger.addBuildLogEntry("Sending " + info);
            if (useProxy(commonTaskContext)) {
                ConnectionParams connectionParams = getConnectionParams(commonTaskContext);
                buildLogger.addBuildLogEntry("Using proxy " + connectionParams);
                notifier.notify(info, connectionParams, buildLogger);
            } else {
                notifier.notify(info, buildLogger);
            }
        } catch (NewRelicApiException e) {
            return TaskResultBuilder.newBuilder(commonTaskContext).failedWithError().build();
        }

        return TaskResultBuilder.newBuilder(commonTaskContext).success().build();
    }

    private String getUser(CommonTaskContext commonTaskContext) {
        TriggerReason trigger = commonTaskContext.getCommonContext().getTriggerReason();
        String triggerKey = trigger.getKey();
        if ("com.atlassian.bamboo.plugin.system.triggerReason:ManualBuildTriggerReason".equals(trigger)) {
            ManualBuildTriggerReason manualTrigger = (ManualBuildTriggerReason) trigger;
            return manualTrigger.getUserName();
        } else if ("com.atlassian.bamboo.plugin.system.triggerReason:RerunBuildTriggerReason".equals(trigger)) {
            RerunBuildTriggerReason rerunTrigger = (RerunBuildTriggerReason) trigger;
            return rerunTrigger.getUserName();
        } else if ("com.atlassian.bamboo.plugin.system.triggerReason:CodeChangedTriggerReason".equals(triggerKey)) {
            return "Triggered by code change";
        } else if ("com.atlassian.bamboo.plugin.system.triggerReason:ChildDependencyTriggerReason".equals(triggerKey)) {
            return "Triggered by child dependency build";
        } else if ("com.atlassian.bamboo.plugin.system.triggerReason:DependencyTriggerReason".equals(triggerKey)) {
            return "Triggered by dependency build";
        } else if ("com.atlassian.bamboo.plugin.system.triggerReason:ScheduledTriggerReason".equals(triggerKey)) {
            return "Scheduled";
        } else {
            return trigger.getName();
        }
    }

    private String createDeploymentDescription(DeploymentTaskContext deploymentContext) {
        String version = deploymentContext.getDeploymentContext().getDeploymentVersion().getName();
        String environment = deploymentContext.getDeploymentContext().getEnvironmentName();
        String projectName = deploymentContext.getDeploymentContext().getDeploymentProjectName();

        StringBuilder description = new StringBuilder();
        description.append("Deployment of version ");
        description.append(version);
        description.append(" to environment ");
        description.append(environment);
        description.append(" from Bamboo deployment project ");
        description.append(projectName);
        description.append(".");
        return description.toString();
    }

    private String createBuildDescription(TaskContext taskContext) {
        String planName = taskContext.getBuildContext().getPlanName();
        String buildResultKey = taskContext.getBuildContext().getBuildResultKey();
        String triggerSentence = taskContext.getCommonContext().getTriggerReason().getNameForSentence();

        StringBuilder description = new StringBuilder();
        description.append("Deployment executed from Bamboo plan ");
        description.append(planName);
        description.append(" (");
        description.append(buildResultKey);
        description.append(") ");
        description.append(". Plan ");
        description.append(triggerSentence);
        description.append(".");
        return description.toString();
    }

    private void populateUserSuppliedData(CommonTaskContext commonTaskContext, DeploymentInfo info) {
        String apiKey = getApiKey(commonTaskContext);
        String appName = commonTaskContext.getConfigurationMap().get(APP_NAME);
        info.setApiKey(apiKey);
        info.setAppName(appName);
    }

}
