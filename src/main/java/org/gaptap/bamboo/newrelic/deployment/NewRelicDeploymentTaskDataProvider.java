/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment;

import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.*;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.bamboo.serialization.WhitelistedSerializable;
import com.atlassian.bamboo.task.runtime.RuntimeTaskDefinition;
import org.gaptap.bamboo.newrelic.deployment.admin.NewRelicAdminService;
import org.gaptap.bamboo.newrelic.deployment.admin.SharedApiKey;
import org.gaptap.bamboo.newrelic.deployment.admin.SharedProxyServer;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.task.RuntimeTaskDataProvider;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.v2.build.CommonContext;

/**
 * @author David Ehringer
 */
public class NewRelicDeploymentTaskDataProvider implements RuntimeTaskDataProvider {

    public static final String SHARED_API_KEY = "sharedApiKey";

    public static final String SHARED_PROXY_HOST = "sharedProxyHost";
    public static final String SHARED_PROXY_PORT = "sharedProxyPort";

    private final NewRelicAdminService newRelicAdminService;

    public NewRelicDeploymentTaskDataProvider(NewRelicAdminService newRelicAdminService) {
        this.newRelicAdminService = newRelicAdminService;
    }

    @Override
    public Map<String, String> populateRuntimeTaskData(@NotNull TaskDefinition taskDefinition,
            @NotNull CommonContext commonContext) {
        Map<String, String> data = new HashMap<String, String>();
        String apiKeyOption = taskDefinition.getConfiguration().get(API_KEY_OPTION);
        if (API_KEY_OPTION_SHARED.equals(apiKeyOption)) {
            String sharedApiKeyId = taskDefinition.getConfiguration().get(SHARED_API_KEY_ID);
            SharedApiKey sharedApiKey = newRelicAdminService.getSharedApiKey(Integer.parseInt(sharedApiKeyId));
            data.put(SHARED_API_KEY, sharedApiKey.getApiKey());
        }
        String proxyOption = taskDefinition.getConfiguration().get(PROXY_OPTION);
        if (PROXY_OPTION_SHARED.equals(proxyOption)) {
            String sharedProxyId = taskDefinition.getConfiguration().get(SHARED_PROXY_ID);
            SharedProxyServer sharedProxyServer = newRelicAdminService.getSharedProxyServer(Integer
                    .parseInt(sharedProxyId));
            data.put(SHARED_PROXY_HOST, sharedProxyServer.getHost());
            data.put(SHARED_PROXY_PORT, String.valueOf(sharedProxyServer.getPort()));
        }
        return data;
    }

    @Override
    public void processRuntimeTaskData(@NotNull TaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        // nothing to do
    }

    @Override
    public void processRuntimeTaskData(@NotNull RuntimeTaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        // Nothing to do
    }

    @NotNull
    @Override
    public Map<String, WhitelistedSerializable> createRuntimeTaskData(@NotNull RuntimeTaskDefinition runtimeTaskDefinition, @NotNull CommonContext commonContext) {
        return new HashMap<String, WhitelistedSerializable>();
    }

}
